Base template to make mockups for apps in Ubuntu Touch.

- App Mockups: Open `app.svg` in Inkscape, select layer accordingly.
  - Added tablet layout. You'll need to make visible the group layers and resize the document to adjust to it
  - Rearranged layers, added Dialog element
- Splash Screens: Open `base-app-splash.svg` in Inkscape. You'll find the guides for recommended maximum app icon size for the splash screen.
